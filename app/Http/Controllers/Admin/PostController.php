<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Group;
use App\Models\Post;
use App\Models\Theme;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();
        return view('admin.post.index', [
            'posts' => $posts
        ]);
    }

    public function create()
    {
        $categories = Category::orderBy('created_at', 'DESC')->get();
        $groups = Group::orderBy('number', 'ASC')->get();
        $theme = Theme::orderBy('created_at', 'DESC')->get();
        return view('admin.post.create', [
            'categories' => $categories,
            'groups' => $groups,
            'themes' => $theme
        ]);
    }

    public function store(Request $request)
    {
        $theme =Theme::where(['id' => $request->theme_id])->first();
        if(empty($theme)){
            return redirect()->back()->withError('Тема не существует!');
        }
        $post = new Post();
        $post->title = $request->title;
        $post->title_kz = $request->title_kz;
        $post->title_en = $request->title_en;
        $post->cat_id = $theme->cat_id;
        $post->class_id = $theme->class_id;
        $post->video_frame = $request->video_code;
        $post->video_frame_kz = $request->video_code_kz;
        $post->video_frame_en = $request->video_code_en;
        $post->theme_id = $request->theme_id;

        $post->save();

       // return redirect()->back()->withSuccess('Урок успешно добавлен!');
		return redirect()->route('post.edit',$post->id)->withSuccess('Урок успешно добавлен!');
    }

    public function show(Post $post)
    {
        //
    }

    public function edit(Post $post)
    {
        $categories = Category::orderBy('created_at', 'DESC')->get();
        $groups = Group::orderBy('created_at', 'DESC')->get();
        $themes = Theme::orderBy('created_at', 'DESC')->get();
        return view('admin.post.edit', [
            'post' => $post,
            'categories' => $categories,
            'groups' => $groups,
            'themes' => $themes
        ]);
    }


    public function update(Request $request, Post $post)
    {

        $theme =Theme::where(['id' => $request->theme_id])->first();
        if(empty($theme)){
            return redirect()->back()->withError('Тема не существует!');
        }

        $post->title = $request->title;
        $post->title_kz = $request->title_kz;
        $post->title_en = $request->title_en;
        $post->cat_id = $theme->cat_id;
        $post->video_frame = $request->video_code;
        $post->video_frame_kz = $request->video_code_kz;
        $post->video_frame_en = $request->video_code_en;
        $post->class_id = $theme->class_id;
        $post->theme_id = $request->theme_id;
        $post->save();
        return redirect()->back()->withSuccess('Урок обновлен!');
    }


    public function destroy(Post $post)
    {
        $post->delete();
        return redirect()->back()->withSuccess('Предмет успешно удален!');
    }
}
