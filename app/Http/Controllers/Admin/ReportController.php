<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Report;
use App\Models\WebHostingDoc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{

    public function index()
    {
        $reports = Report::all();
        return view('admin.report.index', ['reports' => $reports]);
    }


    public function create()
    {
        return view('admin.report.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        try {
            DB::beginTransaction();
            $path = '';
            if (isset($data['file'])) {
                if ($request->hasFile('file')) {
                    $file = $request->file('file');
                    $name = time() . '.' . $file->getClientOriginalExtension();
                    $destinationPath = storage_path('/app/public/reports/');
                    $file->move($destinationPath, $name);
                    $path = '/reports/' . $name;

                }
            }
            $data['file'] = $path;
            Report::create($data);
            DB::commit();
            return redirect(route('report.index'))->withSuccess('Отчет успешно добавлен!');
        } catch (\Exception $exception) {
            DB::rollBack();
            dd($exception->getMessage());
//            return redirect(route('report.index'));
        }
    }


    public function edit(Report $report)
    {
        return view('admin.report.edit', ['report' => $report]);
    }


    public function update(Request $request, Report $report)
    {
        $data = $request->except(['_token', '_method']);
        try {
            DB::beginTransaction();
            $path = null;
            if (isset($data['file'])) {
                if ($request->hasFile('file')) {
                    $file = $request->file('file');
                    $name = time() . '.' . $file->getClientOriginalExtension();
                    $destinationPath = storage_path('/app/public/reports/');
                    $file->move($destinationPath, $name);
                    $path = '/reports/' . $name;

                }
            }
            if ($path) {
                $data['file'] = $path;
            }

            $report->update($data);
            DB::commit();
            return redirect(route('report.index'))->withSuccess('Отчет успешно добавлен!');
        } catch (\Exception $exception) {
            DB::rollBack();
            return [
                'success' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function destroy(Report $report)
    {
        $report->delete();
        return redirect()->back()->withSuccess('Отчет успешно удалена!');
    }
}
