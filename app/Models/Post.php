<?php

namespace App\Models;

use App\filters\QueryFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'title_kz',
        'title_en',
        'video_frame',
        'video_frame_kz',
        'video_frame_en',
        'cat_id',
        'class_id',
        'theme_id',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class , 'cat_id');
    }

    public function scopeFilter(Builder $builder, QueryFilter $filter){
        return $filter->apply($builder);
    }
}
