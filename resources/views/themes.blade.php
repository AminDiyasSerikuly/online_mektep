@extends('layouts.main_layout')
@section('title_page', 'Темы')
@section('content')
    <section class="searchPage">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8">
                    <h2 class="pageTitle">
                        @if(session('locale') == 'ru')
                            {{$sub}}
                        @endif

                        @if(session('locale') == 'en')
                            {{$cat_lang['title_en']}}
                        @endif

                        @if(session('locale') == 'kz')
                            {{$cat_lang['title_kz']}}
                        @endif
                        <span class="pageTitle__additional">
                <span class="pageTitle__additional-el">{{$class}}   @lang('main.class') </span>
              </span>
                    </h2>
                    <div class="searchResults">
                        @if(count($themes) == 0)
                            <p>@lang('main.theme_not_founded')</p>
                        @endif
                        @foreach($themes as $theme)
                            @foreach($groups as $group)
                                @if($group->id == $theme->class_id)

                                    @foreach($categories as $cat)
                                        @if($theme->cat_id == $cat->id && $cat->title == "Биология")
                                                @if(session('locale') == "ru" && isset($theme->title))
                                                    <a class="searchResults__el searchResults__el--turquoise"
                                                       href="{{route('catalog', [$class, $theme->id])}}">
                                                        <span class="searchResults__el-title">{{$theme->title}}</span>
                                                        <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) @lang('main.class') @endif</span>
                                                    </a>
                                                @endif
                                                @if(session('locale') == "en" && isset($theme->title_en))
                                                    <a class="searchResults__el searchResults__el--turquoise"
                                                       href="{{route('catalog', [$class, $theme->id])}}">
                                                        <span class="searchResults__el-title">{{$theme->title_en}}</span>
                                                        <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) @lang('main.class') @endif</span>
                                                    </a>
                                                @endif
                                                @if(session('locale') == "kz" && isset($theme->title_kz))
                                                    <a class="searchResults__el searchResults__el--turquoise"
                                                       href="{{route('catalog', [$class, $theme->id])}}">
                                                        <span class="searchResults__el-title">{{$theme->title_kz}}</span>
                                                        <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) @lang('main.class') @endif</span>
                                                    </a>
                                                @endif

                                        @endif

                                        @if($theme->cat_id == $cat->id && $cat->title == "Математика")
                                                @if(session('locale') == "ru" && isset($theme->title))
                                                    <a class="searchResults__el searchResults__el--red"
                                                       href="{{route('catalog', [$class, $theme->id])}}">
                                                        <span class="searchResults__el-title">{{$theme->title}}</span>
                                                        <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) @lang('main.class') @endif</span>
                                                    </a>
                                                @endif
                                                @if(session('locale') == "en" && isset($theme->title_en))
                                                    <a class="searchResults__el searchResults__el--red"
                                                       href="{{route('catalog', [$class, $theme->id])}}">
                                                        <span class="searchResults__el-title">{{$theme->title_en}}</span>
                                                        <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) @lang('main.class') @endif</span>
                                                    </a>
                                                @endif
                                                @if(session('locale') == "kz" && isset($theme->title_kz))
                                                    <a class="searchResults__el searchResults__el--red"
                                                       href="{{route('catalog', [$class, $theme->id])}}">
                                                        <span class="searchResults__el-title">{{$theme->title_kz}}</span>
                                                        <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) @lang('main.class') @endif</span>
                                                    </a>
                                                @endif

                                        @endif


                                        @if($theme->cat_id == $cat->id && $cat->title == "Химия")

                                                @if(session('locale') == "ru" && isset($theme->title))
                                                    <a class="searchResults__el searchResults__el--violet"
                                                       href="{{route('catalog', [$class, $theme->id])}}">
                                                        <span class="searchResults__el-title">{{$theme->title}}</span>
                                                        <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) @lang('main.class') @endif</span>
                                                    </a>
                                                @endif
                                                @if(session('locale') == "en" && isset($theme->title_en))
                                                    <a class="searchResults__el searchResults__el--violet"
                                                       href="{{route('catalog', [$class, $theme->id])}}">
                                                        <span class="searchResults__el-title">{{$theme->title_en}}</span>
                                                        <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) @lang('main.class') @endif</span>
                                                    </a>
                                                @endif
                                                @if(session('locale') == "kz" && isset($theme->title_kz))
                                                    <a class="searchResults__el searchResults__el--violet"
                                                       href="{{route('catalog', [$class, $theme->id])}}">
                                                        <span class="searchResults__el-title">{{$theme->title_kz}}</span>
                                                        <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) @lang('main.class') @endif</span>
                                                    </a>
                                                @endif

                                        @endif


                                        @if($theme->cat_id == $cat->id && $cat->title == "Физика")
                                                @if(session('locale') == "ru" && isset($theme->title))
                                                    <a class="searchResults__el searchResults__el--blue"
                                                       href="{{route('catalog', [$class, $theme->id])}}">
                                                        <span class="searchResults__el-title">{{$theme->title}}</span>
                                                        <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) @lang('main.class') @endif</span>
                                                    </a>
                                                @endif
                                                @if(session('locale') == "en" && isset($theme->title_en))
                                                    <a class="searchResults__el searchResults__el--blue"
                                                       href="{{route('catalog', [$class, $theme->id])}}">
                                                        <span class="searchResults__el-title">{{$theme->title_en}}</span>
                                                        <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) @lang('main.class') @endif</span>
                                                    </a>
                                                @endif
                                                @if(session('locale') == "kz" && isset($theme->title_kz))
                                                    <a class="searchResults__el searchResults__el--blue"
                                                       href="{{route('catalog', [$class, $theme->id])}}">
                                                        <span class="searchResults__el-title">{{$theme->title_kz}}</span>
                                                        <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) )@lang('main.class') @endif</span>
                                                    </a>
                                                @endif

                                        @endif

                                        @if($theme->cat_id == $cat->id && $cat->title == "Алгебра")
                                            @if(session('locale') == "ru" && isset($theme->title))
                                                <a class="searchResults__el searchResults__el--pink"
                                                   href="{{route('catalog', [$class, $theme->id])}}">
                                                    <span class="searchResults__el-title">{{$theme->title}}</span>
                                                    <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) )@lang('main.class') @endif</span>
                                                </a>
                                            @endif
                                            @if(session('locale') == "en" && isset($theme->title_en))
                                                <a class="searchResults__el searchResults__el--pink"
                                                   href="{{route('catalog', [$class, $theme->id])}}">
                                                    <span class="searchResults__el-title">{{$theme->title_en}}</span>
                                                    <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) )@lang('main.class') @endif</span>
                                                </a>
                                            @endif
                                            @if(session('locale') == "kz" && isset($theme->title_kz))
                                                <a class="searchResults__el searchResults__el--pink"
                                                   href="{{route('catalog', [$class, $theme->id])}}">
                                                    <span class="searchResults__el-title">{{$theme->title_kz}}</span>
                                                    <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) )@lang('main.class') @endif</span>
                                                </a>
                                            @endif

                                        @endif


                                        @if($theme->cat_id == $cat->id && $cat->title == "Геометрия")
                                            @if(session('locale') == "ru" && isset($theme->title))
                                                <a class="searchResults__el searchResults__el--orange"
                                                   href="{{route('catalog', [$class, $theme->id])}}">
                                                    <span class="searchResults__el-title">{{$theme->title}}</span>
                                                    <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) )@lang('main.class') @endif</span>
                                                </a>
                                            @endif
                                            @if(session('locale') == "en" && isset($theme->title_en))
                                                <a class="searchResults__el searchResults__el--orange"
                                                   href="{{route('catalog', [$class, $theme->id])}}">
                                                    <span class="searchResults__el-title">{{$theme->title_en}}</span>
                                                    <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) )@lang('main.class') @endif</span>
                                                </a>
                                            @endif
                                            @if(session('locale') == "kz" && isset($theme->title_kz))
                                                <a class="searchResults__el searchResults__el--orange"
                                                   href="{{route('catalog', [$class, $theme->id])}}">
                                                    <span class="searchResults__el-title">{{$theme->title_kz}}</span>
                                                    <span class="searchResults__el-class">{{$group->number}}   @if(is_numeric($group->number)) )@lang('main.class') @endif</span>
                                                </a>
                                            @endif

                                        @endif



                                    @endforeach

                                @endif
                            @endforeach
                        @endforeach
                    </div>
                    @if(count($themes) > 0)
                        {{ $themes->links('pagination.index') }}
                    @endif
                    {{--                    There are been pagination--}}
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    @if($video != null)
                        <div class="categoryVideo categoryVideo--blue categoryVideo--medium">
                            @foreach($themes as $theme)
                                @if($theme->id == $video['theme_id'])
                                    @foreach($groups as $group)
                                        @if($group->id == $video['class_id'])
                                            <a href="{{route('video', [$group->number, $theme->title, $video['id']])}}"
                                               class="categoryVideo__imgContainer">

                                                {{--                            <img src="/img/maths.jpg" class="categoryVideo__img" alt="#">--}}
                                                {{--                            <span class="categoryVideo__play"></span>--}}
                                                {!! $video['video_frame'] !!}
                                                <span class="categoryVideo__class">{{$group->number}}</span>
                                            </a>
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                            @foreach($themes as $theme)
                                @if($theme->id == $video['theme_id'])
                                    @foreach($groups as $group)
                                        @if($group->id == $video['class_id'])
                                            <h3 class="categoryVideo__title"><a
                                                        href="{{route('video', [$group->number, $theme->title, $video['id']])}}"
                                                        class="categoryVideo__title-link">{{$video['title']}}</a></h3>
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>

@endsection
