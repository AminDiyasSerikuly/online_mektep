@extends('layouts.admin_layout')
@section('title_page', 'Документация')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Документация</h1>
                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
       <h4>Как добавить видео?</h4>
        <p>
            <ul>
                <li>0. Предварительно нужно добавить тему для видео, класс и предмет</li>
                <li>1. В панели администратора переходим во вкладку 'Видеозаписи'->'Добавить видеозапись'</li>
                <li>2. Вводим название видео, указывем тему, класс и предмет</li>
                <li>3. В поле "код видео" нужно вставить код с YouTube</li>
                <li>
                Открываем на YouTube видео. Внизу находится кнопка "поделиться", кликакем на нее.
                <br>
                <img src="/img/1.png" alt="1" width="700px">
                <br>
                Далее нажимаем "Встроить"
                <br>
                <img src="/img/2.png" alt="2" width="700px">
                <br>
                и копируем код
                <br>
                <img src="/img/3.png" alt="3" width="700px">
                <br>
                </li>
             </ul>
        </p>
    </section>
    <!-- /.content -->
@endsection
