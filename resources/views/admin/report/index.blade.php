@extends('layouts.admin_layout')
@section('title_page', 'Отчеты')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Все отчеты</h1>
                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <div class="card">
        <div class="card-header">
            <div class="div">
                <a href="{{route('report.create')}}" class="btn btn-success">
                    Добавить отчет
                </a>
            </div>
        </div>
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Заголовок</th>
                    <th>Файл</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                @foreach($reports as $item)
                    <tr>
                        <td>{{$item->title}}</td>
                        <td>
                            <a href="{{asset('storage'.$item->file)}}" download>
                                {{$item->title}}
                            </a>
                        </td>
                        <td>
                            <div class="row">
                                <a href="{{route('report.edit', $item)}}"
                                   class="btn btn-sm btn-primary">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <form action="{{route('report.destroy', $item->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="ml-2 btn btn-sm btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop


@section('plugins.Datatables', true)
@section('js')
    <script src="{{asset("vendor/datatables/js/jquery.dataTables.min.js")}}"></script>
    <script src="{{asset("vendor/datatables/js/dataTables.bootstrap4.min.js")}}"></script>
    <script src="{{asset("vendor/datatables-plugins/responsive/js/dataTables.responsive.min.js")}}"></script>
    <script src="{{asset("vendor/datatables-plugins/responsive/js/responsive.bootstrap4.min.js")}}"></script>
    <script src="{{asset("vendor/datatables-plugins/buttons/js/dataTables.buttons.min.js")}}"></script>
    <script src="{{asset("vendor/datatables-plugins/buttons/js/buttons.bootstrap4.min.js")}}"></script>
    <script src="{{asset("vendor/datatables-plugins/jszip/jszip.min.js")}}"></script>
    <script src="{{asset("vendor/datatables-plugins/pdfmake/pdfmake.min.js")}}"></script>
    <script src="{{asset("vendor/datatables-plugins/pdfmake/vfs_fonts.js")}}"></script>
    <script src="{{asset("vendor/datatables-plugins/buttons/js/buttons.html5.min.js")}}"></script>
    <script src="{{asset("vendor/datatables-plugins/buttons/js/buttons.print.min.js")}}"></script>
    <script src="{{asset("vendor/datatables-plugins/buttons/js/buttons.colVis.min.js")}}"></script>
    <script src="{{asset("bs-custom-file-input/bs-custom-file-input.min.js")}}"></script>
    <script>
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": false,
                "autoWidth": false,
                "responsive": false,
            });
        });
    </script>
@stop
