@extends('layouts.admin_layout')
@section('title_page', 'Редактировать отчет')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Редактировать отчет</h1>
                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="card">
        <div class="card-body">
            <form action="{{route('report.update', $report)}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Заголовок</label>
                            <input value="{{$report->title}}" name="title" type="text" class="form-control"
                                   placeholder="Заголовок">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="customFile">  Текущий файл: <a href="{{asset('storage' . $report->file)}}" download>
                                    {{$report->file}}
                                </a></label>
                            <div class="custom-file">
                                <input  name="file" type="file" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">
                                    Выберите файл
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Сохранить</button>
            </form>
        </div>
    </div>
@endsection
