@extends('layouts.admin_layout')
@section('title_page', 'Поиск')
@section('content')
<form action="{{route('searching_by_theme')}}" method="GET" class="container-fluid mt-3 mb-3">
	@csrf
	<div class="form-inline">
		<div class="input-group">
<input type="text" id="s" name="s" placeholder="Ваш запрос" value="{{$request->s ?? ''}}">
<input type="submit" value="Поиск">
		</div>
		</div>
	</form>

 <section class="content">
        <div class="container-fluid">
            <div class="card">
                @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                        <h4><i class="icon fa fa-check"></i>{{session('success')}}</h4>
                    </div>
                @endif


				@if(isset($themes))

                <div class="card-body p-0">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th style="width: 1%">
                                Id
                            </th>
                            <th style="width: 20%">
                                Название (ru)
                            </th>

                            <th style="width: 20%">
                                Название (kz)
                            </th>

                            <th style="width: 20%">
                                Название (en)
                            </th>

                            <th>Категория</th>

                            <th>Дата добавления</th>

                            <th style="width: 20%">
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($themes as $post)
                            <tr>
                                <td>
                                    {{$post->id}}
                                </td>
                                <td>
                                    {{$post->title}}
                                </td>
                                <td>
                                    {{$post->title_kz}}
                                </td>
                                <td>
                                    {{$post->title_en}}
                                </td>
                                <td>
                                @if(isset($post->category['title']))
                                {{$post->category['title']}}
                                @endif
                                </td>
                                <td>{{$post->created_at}}</td>

                                <td class="project-actions text-right">
                                    <a class="btn btn-info btn-sm" href="{{route('theme.edit', $post->id)}}">
                                        <i class="fas fa-pencil-alt">
                                        </i>
                                        Изменить
                                    </a>
                                    <form action="{{route('theme.destroy', $post->id) }}" class="btn" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="hidden" name="_method" value="delete" />
                                        <button type="submit" class="btn btn-danger btn-sm delete-btn" href="#">
                                            <i class="fas fa-trash">
                                            </i>
                                            Удалить
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach

                            </tr>
                        </tbody>
                    </table>
                </div>
	 			@endif
                <!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection
