@extends('layouts.admin_layout')
@section('title_page', 'Добавить урок')
@section('content')
    <style>
        .btn-tab{
            border: 1px solid rgba(0,0,0,.1);
            border-radius: 0.5rem;
            font-weight: 600;
            padding: 1rem;
            background-color: #c2c7d0;
            cursor: pointer;
        }
        .lang_ru,.lang_kz,.lang_en{
            padding: 1rem;
        }
        .langs_checks{
            margin-bottom: 1rem;
            text-align: center;
        }
        .langs_checks div{
            display: inline-block;
        }
        .hidden-forms{
            display: none;
        }
    </style>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Добавить урок</h1>
                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary">

                @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                        <h4><i class="icon fa fa-check"></i>{{session('success')}}</h4>
                    </div>
            @endif

            <!-- /.card-header -->
                <!-- form start -->
                <form action="{{route('post.store')}}" method="POST">
                    @csrf
                    <div class="card-body">

                        <div class="form-group">
                            <label>Предмет</label>
                            <select class="form-control" name="cat_id">
                                @foreach($categories as $cat)
                                    <option value="{{$cat['id']}}">{{$cat['title']}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Тема</label>
                            <select class="form-control select2" name="theme_id">
                                @foreach($themes as $theme)
                                    <option value="{{$theme->id}}">{{$theme->title ? '(RU) '.$theme->title : ''}} {{$theme->title_kz ? ' (KZ) '.$theme->title_kz  : ''}} {{$theme->title_en ? ' (EN) '.$theme->title_en : ''}}</option>
                                @endforeach
                            </select>
                        </div>

{{--                        <div class="form-group">--}}
{{--                            <label>Класс</label>--}}
{{--                            <select class="form-control" name="class_id">--}}
{{--                                @foreach($groups as $group)--}}
{{--                                    <option value="{{$group->id}}" >{{$group->number}}</option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
{{--                        </div>--}}

{{--                        <div class="langs_checks">--}}
{{--                            <p>На каких языках записан данный урок</p>--}}
{{--                            <div class="checkbox icheck-primary">--}}
{{--                                <input class="form-check-input" value="ru" type="checkbox" id="ru_check" >--}}
{{--                                <label class="form-check-label" for="ru_check">Русский</label>--}}
{{--                            </div>--}}
{{--                            <div class="checkbox icheck-primary">--}}
{{--                                <input class="form-check-input" value="kz" type="checkbox" id="kz_check">--}}
{{--                                <label class="form-check-label" for="kz_check">Казахский</label>--}}
{{--                            </div>--}}
{{--                            <div class="checkbox icheck-primary">--}}
{{--                                <input class="form-check-input" value="en" type="checkbox" id="en_check">--}}
{{--                                <label class="form-check-label" for="en_check">Английский</label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div style="font-size: small; font-style: italic">
                            *Если название темы прописано только на русском языке, то можно добавлять видео только в русскую версию.
                            Если на русском и казахском, то можно добавить видео и в русскую, и в казахскую, и т.д. </br>
                            Чтобы добавить название темы на том или ином языке, зайдите в раздел "Темы".*
                        </div>
                        <div class="btn-tab" id="ru" style="display: block; text-align: center">Русская версия</div>
                        <div class="lang_ru" style="display: none">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Название</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" name="title" placeholder="Введите имя предмета">
                            </div>

                            <div class="form-group">
                                <label>Код видео</label>
                                <textarea class="form-control" rows="3" name="video_code" placeholder='<iframe width="560" height="315" src="https://www.youtube.com/embed/FrjD5CZ4a88" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'></textarea>
                            </div>
                        </div>
                        <div class="btn-tab" id="kz" style="display: block; text-align: center">Казахская версия</div>
                        <div class="lang_kz" style="display: none">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Название на казахском </label>
                                <input type="text" class="form-control" id="exampleInputEmail1" name="title_kz" placeholder="Введите название урока">
                            </div>

                            <div class="form-group">
                                <label>Код видео (казахская версия)  </label>
                                <textarea class="form-control" rows="3" name="video_code_kz" placeholder='<iframe width="560" height="315" src="https://www.youtube.com/embed/FrjD5CZ4a88" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>' ></textarea>
                            </div>
                        </div>
                        <div class="btn-tab" id="en" style="display: block; text-align: center">Английская версия</div>
                        <div class="lang_en" style="display: none">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Название на английском </label>
                                <input type="text" class="form-control" id="exampleInputEmail1" name="title_en" placeholder="Введите название урока">
                            </div>

                            <div class="form-group">
                                <label>Код видео (английская версия) </label>
                                <textarea class="form-control" rows="3" name="video_code_en" placeholder='<iframe width="560" height="315" src="https://www.youtube.com/embed/FrjD5CZ4a88" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>' ></textarea>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Обновить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <script src="/admin/plugins/jquery/jquery.min.js"></script>
    <script src="/admin/plugins/select2/js/select2.full.min.js"></script>
    <script src="/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script>

        $('select[name="theme_id"]').on('change', function() {
            check_select();
        });

        function check_select(){
            let str = $( 'select[name=theme_id] option:selected' ).text();
            console.log(str);
            if (str.indexOf("(RU)") < 0){
                $('#ru').css('display', 'none');
                $('.lang_ru').css('display', 'none');
                $('input[name=title]').removeAttr('required');
                $('textarea[name=video_code]').removeAttr('required');
            }else{
                $('#ru').css('display', 'block');
                $('.lang_ru').css('display', 'block');
                $('input[name=title]').attr('required', 'required');
                $('textarea[name=video_code]').attr('required', 'required');
            }
            if (str.indexOf("(KZ)") < 0){
                $('#kz').css('display', 'none');
                $('.lang_kz').css('display', 'none');
                $('input[name=title_kz]').removeAttr('required');
                $('textarea[name=video_code_kz]').removeAttr('required');
            }else{
                $('#kz').css('display', 'block');
                $('.lang_kz').css('display', 'block');
                $('input[name=title_kz]').attr('required', 'required');
                $('textarea[name=video_code_kz]').attr('required', 'required');
            }
            if (str.indexOf("(EN)") < 0){
                $('#en').css('display', 'none');
                $('.lang_en').css('display', 'none');
                $('input[name=title_en]').removeAttr('required');
                $('textarea[name=video_code_en]').removeAttr('required');
            }else{
                $('#en').css('display', 'block');
                $('.lang_en').css('display', 'block');
                $('input[name=title_en]').attr('required', 'required');
                $('textarea[name=video_code_en]').attr('required', 'required');
            }
        }

        $(document).ready(function() {


            check_select();

            $('select[name="theme_id"]').on('change', function() {
                check_select();
            });

            $('form .btn-tab').click(function () {
                let lang = $(this).attr('id');
                $('.lang_'+lang).slideToggle('slow');
            })

            $('.langs_checks input[type=checkbox]').each(function () {
                check_checkbox($(this));
                console.log('peace')
            })

            $('.langs_checks input[type=checkbox]').change(function () {
                check_checkbox($(this));
            })

            $('select[name="cat_id"]').on('change', function(){
                let cat_id = $(this).val();
                if(cat_id) {
                    $.ajax({
                        url: '/admin_panel/getThemes/'+cat_id,
                        type:"GET",
                        dataType:"json",
                        // beforeSend: function(){
                        //     $('#loader').css("visibility", "visible");
                        // },
                        success:function(data) {
                            // $.each(data, function(key, value) {
                            //     console.log(value['title']);
                            // })
                            $('select[name="theme_id"]').empty();

                            $.each(data, function(key, value){
                                var theme = '';
                                if(value['title'] != null){
                                    theme = theme + '(RU) ' + value['title'];
                                }if(value['title_kz'] != null){
                                    theme = theme + '(KZ) ' + value['title_kz'];
                                }if(value['title_en'] != null){
                                    theme = theme + '(EN) ' + value['title_en'];
                                }
                                $('select[name="theme_id"]').append('<option value="'+ value['id'] +'">' + theme  + '</option>');

                            });
                        },
                    });
                } else {

                }

            });

        });

        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        });
    </script>
@endsection
