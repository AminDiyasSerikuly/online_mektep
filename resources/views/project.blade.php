@extends('layouts.main_layout')
@section('title_page', 'Проект')
@section('content')

    <div class="container">
        <div class="categoriesList__el">
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    <span> &nbsp;@lang('main.reports')</span>
                </button>
                <div class="dropdown-menu dropdown-menu-left">
                    @foreach($reports as $report)
                        <a href="{{asset('storage' . $report->file)}}" target="_blank" class="dropdown-item" type="button">{{$report->title}}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
